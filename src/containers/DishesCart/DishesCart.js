import React, {useEffect, useState} from 'react';
import {Box, Collapse, IconButton} from "@mui/material";
import {showOrderAlert} from "../../store/actions/cartActions";
import {useDispatch, useSelector} from "react-redux";
import Alert from "@mui/material/Alert";
import CloseIcon from '@mui/icons-material/Close';
import DishesCartResults from "../DishesCartResults/DishesCartResults";
import CartBasket from "../CartBasket/CartBasket";
import './DishesCart.css';


const DishesCart = () => {
   const dispatch = useDispatch();
   const [isAlertMessage, setIsAlertMessage] = useState(true);
   const isAlertShown = useSelector((state) => state.cart.isAlertShown);

   useEffect(() => {
      if (isAlertShown === true) {
         setIsAlertMessage(true);
      } else if (isAlertShown === false) {
         setIsAlertMessage(false);
      }
      if (isAlertShown !== null){
         const alertTimeout = setTimeout(() => {
            dispatch(showOrderAlert(null));
         },4000);
         return () => {
            clearTimeout(alertTimeout);
         }
      }
   },[dispatch, isAlertShown]);

   return (
     <fieldset className="food-menu-item-cart">
        <legend>Cart</legend>
        <CartBasket/>
        <DishesCartResults />
        <Box sx={{transform: 'translateX(-50%)'}} position={"fixed"} left={'50%'} top={'10px'}>
           <Collapse in={isAlertShown !== null}>
              <Alert
                action={
                   <IconButton
                     color="inherit"
                     size="small"
                     onClick={() => {
                        dispatch(showOrderAlert(null));
                     }}
                   >
                      <CloseIcon fontSize="inherit"/>
                   </IconButton>
                }
                severity={isAlertMessage ?  "success": "error" }
              >
                 {isAlertMessage ? 'Thank you for your purchase':'Task failed successfully'}
              </Alert>
           </Collapse>
        </Box>
     </fieldset>
   );
};

export default DishesCart;