import React, {useState} from 'react';
import {Grid} from "@mui/material";
import Button from "@mui/material/Button";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import OrderModal from "../../components/UI/OrderModal/OrderModal";
import OrderForm from "../../components/OrderForm/OrderForm";
import {orderDishes} from "../../store/actions/cartActions";
import {shallowEqual, useDispatch, useSelector} from "react-redux";

const DishesCartResults = () => {
   const dispatch = useDispatch();
   const [orderModalIsOpen, setOrderModalIsOpen] = useState(false);
   const {
      cartBasket,
      deliveryPrice,
      cartSum,
      loading,
   } = useSelector((state) => ({
      cartBasket: state.cart.cartBasket,
      deliveryPrice: state.cart.deliveryPrice,
      cartSum: state.cart.cartSum,
      loading: state.cart.loading,
   }), shallowEqual);
   return (
     <Grid container direction={"column"} sx={{borderTop: '3px solid gainsboro'}}>
        <Grid item mt={2}>
           Доставка: {deliveryPrice} KGS
        </Grid>
        <Grid item mt={1}>
           Итого: {cartSum} KGS
        </Grid>
        <Grid item textAlign={"center"} my={3}>
           <Button
             onClick={() => setOrderModalIsOpen(true)}
             variant={'contained'}
             color="success"
             disabled={cartSum === 0}
             aria-label="add to shopping cart"
             size={"large"}>
              Place order
              <AddShoppingCartIcon sx={{marginLeft: '10px'}}/>
           </Button>
           <OrderModal open={orderModalIsOpen} handleClose={() => setOrderModalIsOpen(false)}>
              <OrderForm
                cartBasket={cartBasket}
                sendOrder={(order) => dispatch(orderDishes(order, () => setOrderModalIsOpen(false)))}
                loading={loading}
              />
           </OrderModal>
        </Grid>
     </Grid>
   );
};

export default DishesCartResults;