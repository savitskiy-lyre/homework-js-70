import React from 'react';
import {Grid, IconButton, Paper} from "@mui/material";
import {removeItemFromCart, resetItemFromCart} from "../../store/actions/cartActions";
import DeleteForeverOutlinedIcon from "@mui/icons-material/DeleteForeverOutlined";
import DeleteIcon from "@mui/icons-material/Delete";
import {useDispatch, useSelector} from "react-redux";
import {styled} from "@mui/material/styles";

const Item = styled(Paper)(({theme}) => ({
   ...theme.typography.body2,
   padding: theme.spacing(1),
   textAlign: 'center',
   color: theme.palette.text.primary,
}));

const CartBasket = () => {
   const dispatch= useDispatch();
   const cartBasket = useSelector(state => state.cart.cartBasket);

   return cartBasket && (
       <div>
          {Object.keys(cartBasket).map((key) => {
             return (
               <Grid container spacing={2} key={Math.random()}>
                  <Grid item xs={6} alignContent='center'>
                     <Item elevation={0}>{key}</Item>
                  </Grid>
                  <Grid item xs={2}>
                     <Item elevation={0}>{' x' + cartBasket[key]}</Item>
                  </Grid>
                  <Grid item xs={2} container justifyContent={'right'}>
                     <IconButton
                       aria-label="delete"
                       onClick={() => dispatch(resetItemFromCart(key))}
                     >
                        <DeleteForeverOutlinedIcon/>
                     </IconButton>
                  </Grid>
                  <Grid item xs={2} container justifyContent={'right'}>
                     <IconButton
                       sx={{margin: "0 10px"}}
                       aria-label="delete"
                       onClick={() => dispatch(removeItemFromCart(key))}
                     >
                        <DeleteIcon/>
                     </IconButton>
                  </Grid>
               </Grid>
             );
          })}
       </div>
   );
};

export default CartBasket;