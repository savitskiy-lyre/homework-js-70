import {axiosApi} from "../../axiosApi";
import {DISHES_URL} from "../../config";

export const FETCH_DISHES_REQUEST = 'FETCH_DISHES_REQUEST';
export const FETCH_DISHES_SUCCESS = 'FETCH_DISHES_SUCCESS';
export const FETCH_DISHES_FAILURE = 'FETCH_DISHES_FAILURE';

export const fetchDishesRequest = () => ({type: FETCH_DISHES_REQUEST});
export const fetchDishesSuccess = (data) => ({type: FETCH_DISHES_SUCCESS, payload: data});
export const fetchDishesFailure = (error) => ({type: FETCH_DISHES_FAILURE, payload: error});

export const fetchDishes = () => {
   return async (dispatch) => {
      try {
         dispatch(fetchDishesRequest());
         const {data} = await axiosApi.get(DISHES_URL);
         Object.keys(data).forEach((key) => {
            data[key].id = Math.random();
         })
         dispatch(fetchDishesSuccess(data));
      } catch (error) {
         dispatch(fetchDishesFailure(error));
      }

   }
}