import './App.css';
import {useEffect} from "react";
import BackdropPreloader from "./components/UI/BackdropPreloader/BackdropPreloader";
import {useDispatch, useSelector} from "react-redux";
import {fetchDishes} from "./store/actions/dishesActions";
import {addItemToCart, calculateCartSum} from "./store/actions/cartActions";
import DishesCart from "./containers/DishesCart/DishesCart";
import DishesMenu from "./components/DishesMenu/DishesMenu";

const App = () => {
   const dispatch = useDispatch();
   const dishesMenu = useSelector((state) => state.dishes.dishesMenu);
   const loading = useSelector((state) => state.dishes.loading);
   const cartBasket = useSelector((state) => state.cart.cartBasket);

   useEffect(() => {
      dispatch(calculateCartSum(dishesMenu));
   }, [dispatch, cartBasket, dishesMenu])

   useEffect(() => {
      dispatch(fetchDishes());
   }, [dispatch])
   return (
     <div className="App">
        {loading && (
          <BackdropPreloader option={{type: Math.floor(Math.random() * 10)}}/>
        )}
        {dishesMenu && (
          <div className={'food-menu-page'}>
             <DishesMenu dishesMenu={dishesMenu} addItem={(name) => dispatch(addItemToCart(name))}/>
             <DishesCart/>
          </div>
        )}
     </div>
   );
};

export default App;
